TEMPLATE = app
CONFIG += console c++11
CONFIG -= app_bundle
CONFIG -= qt

LIBS += -lboost_system -lcrypto -lssl -lcpprest -lncurses -lmenu

SOURCES += \
        main.cpp \
    api_request.cpp \
    apiresponse.cpp \
    loginscreen.cpp \
    ui_utils.cpp \
    helpers/accounthelper.cpp \
    helpers/userhelper.cpp \
    entities/user.cpp \
    mainmenu.cpp \
    conversations_screen.cpp \
    helpers/conversationshelper.cpp \
    entities/conversation.cpp \
    entities/conversationslist.cpp

HEADERS += \
    config.h \
    api_request.h \
    apiresponse.h \
    loginscreen.h \
    ui_utils.h \
    helpers/accounthelper.h \
    helpers/userhelper.h \
    entities/user.h \
    mainmenu.h \
    conversations_screen.h \
    helpers/conversationshelper.h \
    entities/conversation.h \
    entities/conversationslist.h
