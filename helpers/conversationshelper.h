/**
 * Conversations helper
 *
 * @author Pierre HUBERT
 */

#pragma once

#include <vector>

class Conversation;
class ConversationsList;

class ConversationsHelper
{
public:
    ConversationsHelper();

    /**
     * Get the list of conversations of the current user
     */
    static ConversationsList GetList();
};
