/**
 * Account helper
 *
 * @author Pierre HUBERT
 */

#pragma once

#include <string>
#include <vector>

enum LoginResult {
    ERROR,
    BAD_PASSWORD,
    TOO_MANY_ATTEMPTS,
    SUCCESS
};

class AccountHelper
{
public:
    AccountHelper();

    static LoginResult Login(const std::string &email,
                      const std::string &pass);


    static bool signedIn();
    static void SignOut();

    static bool refreshUserID();
    static int userID();

    static void setLoginTokens(std::vector<std::string> tokens);
    static std::vector<std::string> loginTokens();

private:
    static int mUserID;
    static std::string mToken1;
    static std::string mToken2;
};
