/**
 * User helper
 *
 * @author Pierre HUBERT
 */

#pragma once

#include <map>
#include <vector>
#include <set>

#include <entities/user.h>

class UserHelper
{
public:
    UserHelper();

    static User GetSingle(int id);
    static UsersList getMultiple(std::vector<int> ids);
    static UsersList getMultiple(std::set<int> ids);

private:
    static std::map<int, User> mCache;
};
