#include <string>

#include "conversationshelper.h"
#include "../api_request.h"
#include "../entities/conversation.h"
#include "../entities/conversationslist.h"

using namespace std;

ConversationsHelper::ConversationsHelper()
{

}

ConversationsList ConversationsHelper::GetList()
{
    const auto response = ApiRequest("conversations/getList").exec();

    if(response.code() != 200)
        throw runtime_error("Get conversations list failed! (code: " + to_string(response.code()));

    ConversationsList list;

    for(auto el : response.array()) {
        auto obj = el.as_object();

        Conversation conv;
        conv.setID(el["ID"].as_integer());
        conv.setID_Owner(el["ID_owner"].as_integer());
        conv.setLastActive(el["last_active"].as_integer());
        conv.setName(el["name"].is_boolean() ? "" : el["name"].as_string());
        conv.setFollowing(el["following"].as_integer() == 1);
        conv.setSawLastMessage(el["saw_last_message"].as_integer() == 1);

        vector<int> members;
        for(auto m : el["members"].as_array())
            members.push_back(stoi(m.as_string()));
        conv.setMembers(members);

        list.push_back(conv);

    }

    return list;
}
