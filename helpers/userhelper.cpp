#include "userhelper.h"

#include <api_request.h>

using namespace std;

std::map<int, User> UserHelper::mCache;

UserHelper::UserHelper()
{

}

User UserHelper::GetSingle(int id)
{
    auto l = vector<int>();
    l.push_back(id);
    return getMultiple(l)[id];
}

UsersList UserHelper::getMultiple(std::vector<int> ids)
{
    UsersList users;
    vector<int> toGet;

    // Check for missing users in cache
    for(auto id : ids) {
        if(users.find(id) == users.end())
            toGet.push_back(id);
        else
            users[id] = users[id];
    }

    // Get required users info
    if(toGet.size() > 0) {

        string ids = "";
        for(auto i : toGet) ids += to_string(i) + ",";

        auto req = ApiRequest("user/getInfoMultiple", true);
        req.addArg("usersID", ids);

        auto res = req.exec();
        if(res.code() != 200)
            throw runtime_error("Could not get the list of users (status code of response: " + to_string(res.code()));


        // Parse results
        const auto obj = res.object();
        for(auto id : toGet) {
            const auto user_obj = obj.at(to_string(id)).as_object();
            User newUser(user_obj.at("userID").as_number().to_int32(),
                         user_obj.at("firstName").as_string(),
                         user_obj.at("lastName").as_string());

            mCache[id] = newUser;
            users[id] = newUser;
        }

    }


    return users;
}

UsersList UserHelper::getMultiple(std::set<int> ids)
{
    return getMultiple(vector<int>(ids.begin(), ids.end()));
}
