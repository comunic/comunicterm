#include "accounthelper.h"

#include "../api_request.h"

using namespace std;

int AccountHelper::mUserID = -1;
string AccountHelper::mToken1;
string AccountHelper::mToken2;


AccountHelper::AccountHelper()
{

}

LoginResult AccountHelper::Login(const std::string &email, const std::string &pass)
{
    auto request = ApiRequest("user/connectUSER", false);
    request.addArg("userMail", email);
    request.addArg("userPassword", pass);

    const auto result = request.exec();

    // Check for error
    switch (result.code()) {
        case 401:
            return LoginResult::BAD_PASSWORD;
        case 429:
            return LoginResult::TOO_MANY_ATTEMPTS;
        case 200:
            break;
        default:
            return LoginResult::ERROR;
    }

    // Save the tokens
    const auto tokens = result.object().at("tokens").as_object();
    AccountHelper::mToken1 = tokens.at("token1").as_string();
    AccountHelper::mToken2 = tokens.at("token2").as_string();

    if(!refreshUserID())
        return ERROR;


    return SUCCESS;
}

bool AccountHelper::signedIn()
{
    return mToken1.length() > 0 && mToken2.length() > 0 && mUserID > 0;
}

void AccountHelper::SignOut()
{
    mToken1 = "";
    mToken2 = "";
    mUserID = -1;
}

bool AccountHelper::refreshUserID()
{
    // Get current user ID
    auto response = ApiRequest("user/getCurrentUserID", true).exec();
    if(response.code() != 200) return false;
    AccountHelper::mUserID = response.object().at("userID").as_number().to_int32();

    return true;
}

int AccountHelper::userID()
{
    return mUserID;
}

void AccountHelper::setLoginTokens(std::vector<string> tokens)
{
    AccountHelper::mToken1 = tokens[0];
    AccountHelper::mToken2 = tokens[1];
}

vector<string> AccountHelper::loginTokens()
{
    auto tokens = vector<string>();
    tokens.push_back(AccountHelper::mToken1);
    tokens.push_back(AccountHelper::mToken2);
    return tokens;
}
