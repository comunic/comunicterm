#include "apiresponse.h"

using namespace web;

ApiResponse::ApiResponse(int code, json::value content)
    : mCode(code), mContent(content)
{

}

json::value ApiResponse::content() const
{
    return mContent;
}

int ApiResponse::code() const
{
    return mCode;
}

json::object ApiResponse::object() const
{
    return mContent.as_object();
}

json::array ApiResponse::array() const
{
    return mContent.as_array();
}
