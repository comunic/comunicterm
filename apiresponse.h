/**
 * API Response
 *
 * @author Pierre HUBERT
 */

#pragma once

#include <cpprest/json.h>

class ApiResponse
{
public:
    ApiResponse(int code, web::json::value content);

    web::json::value content() const;

    int code() const;

    web::json::object object() const;

    web::json::array array() const;

private:
    int mCode;
    web::json::value mContent;
};
