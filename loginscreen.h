/**
 * Login screen
 *
 * @author Pierre HUBERT
 */

#include <string>

class LoginScreen
{
public:
    LoginScreen();

    bool exec(bool startApp = true);

private:
    void getCredentials(std::string &email, std::string &pass);
};
