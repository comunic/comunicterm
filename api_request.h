/**
 * Single API request
 *
 * @author Pierre HUBERT
 */
#pragma once

#include <string>

#include "apiresponse.h"

class ApiRequest
{
public:
    ApiRequest(const std::string &uri, bool needLogin = true);

    void addArg(const std::string &name, const std::string &value);

    ApiResponse exec();

private:
    std::string mURI;
    std::string mReqBody;
};
