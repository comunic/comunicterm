#include <stdlib.h>
#include <string.h>

#include <menu.h>
#include <curses.h>

#include <iostream>

#include <helpers/accounthelper.h>

#include "loginscreen.h"
#include "mainmenu.h"
#include "conversations_screen.h"
#include "ui_utils.h"

using namespace std;


#define ARRAY_SIZE(a) (sizeof(a) / sizeof(a[0]))
#define CTRLD 	4

static char *choices[] = {
    "Conversations",
    "Logout",
    "Quit",
    nullptr
};

void showMainMenu()
{
while(true) {
    ITEM **my_items;
    int c;
    MENU *my_menu;
    WINDOW *my_menu_win;
    int n_choices, i;

    /* Initialize curses */
    initscr();
    start_color();
    cbreak();
    noecho();
    keypad(stdscr, TRUE);
    init_pair(1, COLOR_RED, COLOR_BLACK);
    init_pair(2, COLOR_CYAN, COLOR_BLACK);

    erase();

    /* Create items */
    n_choices = ARRAY_SIZE(choices);
    my_items = static_cast<ITEM **>(calloc(static_cast<size_t>(n_choices), sizeof(ITEM *)));
    for(i = 0; i < n_choices; ++i)
            my_items[i] = new_item(choices[i], choices[i]);

    /* Create menu */
    my_menu = new_menu(static_cast<ITEM **>(my_items));

    /* Create the window to be associated with the menu */
    int w,h;
    getmaxyx(stdscr, w, h);
    my_menu_win = newwin(10, 40, (w-10)/2, (h-40)/2);
    keypad(my_menu_win, TRUE);

    /* Set main window and sub window */
    set_menu_win(my_menu, my_menu_win);
    set_menu_sub(my_menu, derwin(my_menu_win, 6, 38, 3, 1));
    set_menu_format(my_menu, 5, 1);

    /* Set menu mark to the string " * " */
    set_menu_mark(my_menu, " * ");

    /* Print a border around the main window and print a title */
    box(my_menu_win, 0, 0);
    print_in_middle(my_menu_win, 1, 0, 40, "ComunicTerm", COLOR_PAIR(1));
    mvwaddch(my_menu_win, 2, 0, ACS_LTEE);
    mvwhline(my_menu_win, 2, 1, ACS_HLINE, 38);
    mvwaddch(my_menu_win, 2, 39, ACS_RTEE);

    /* Post the menu */
    post_menu(my_menu);
    wrefresh(my_menu_win);

    attron(COLOR_PAIR(2));
    mvprintw(LINES - 2, 0, "");
    mvprintw(LINES - 1, 0, "Arrow Keys to navigate");
    attroff(COLOR_PAIR(2));
    refresh();

    int action = -1;
    while(action == -1)
    {
        c = wgetch(my_menu_win);
        switch(c)
            {	case KEY_DOWN:
                menu_driver(my_menu, REQ_DOWN_ITEM);
                break;
            case KEY_UP:
                menu_driver(my_menu, REQ_UP_ITEM);
                break;
            case KEY_NPAGE:
                menu_driver(my_menu, REQ_SCR_DPAGE);
                break;
            case KEY_PPAGE:
                menu_driver(my_menu, REQ_SCR_UPAGE);
                break;
            case 10:
                auto item = current_item(my_menu);
                action = item->index;
        }
        wrefresh(my_menu_win);
    }

    /* Unpost and free all the memory taken up */
    unpost_menu(my_menu);
    free_menu(my_menu);
    for(i = 0; i < n_choices; ++i)
            free_item(my_items[i]);
    endwin();


    /* Process requested action */
    int o = 0;

    // Open user conversations
    if(o++ == action) {
        showConversationsScreen();
    }

    // Logout
    else if(o++ == action) {
        AccountHelper::SignOut();
        LoginScreen().exec(false);
    }

    // Quit application
    else {
        cout << "Good bye !" << endl;
        exit(0);
    }

}
}
