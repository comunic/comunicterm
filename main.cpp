#include <iostream>

#include <helpers/accounthelper.h>

#include "api_request.h"
#include "loginscreen.h"
#include "mainmenu.h"

using namespace std;

int main(int argc, char **argv)
{
    cout << "Comunic Term (c) Pierre HUBERT" << endl;

    bool onlyGetTokens = false;

    if(argc > 1) {
        string arg = argv[1];

        // Show help
        if(arg == "help") {
            cout << "Usage: " << endl;
            cout << "* help : Show this help" << endl;
            cout << "* getTokens : Get login token ONLY" << endl;
            cout << "* useTokens : Input login tokens (do not show login form)" << endl;
            return 0;
        }

        // Get login tokens
        else if(arg == "getTokens") {
            cout << "Getting only login tokens..." << endl;
            onlyGetTokens = true;
        }

        // Use existing login tokens
        else if(arg == "useTokens") {
            if(argc != 4) {
                cerr << "Need to specify tokens!" << endl;
                return -2;
            }

            // Set tokens
            vector<std::string> tokens;
            tokens.push_back(argv[2]);
            tokens.push_back(argv[3]);
            AccountHelper::setLoginTokens(tokens);

            // Validate them
            if(!AccountHelper::refreshUserID()) {
                cerr << "Could not use login tokens!" << endl;
                return -3;
            }

            // Start directly main main menu
            showMainMenu();

            return 0;
        }

        else {
            cerr << "Unrecognized argument!" << endl;
            return -1;
        }


    }

    // First, we need to sign in user
    if(!AccountHelper::signedIn() && !LoginScreen().exec(!onlyGetTokens)) {
        cerr << "Could not sign in user!" << endl;
        return -1;
    }

    if(onlyGetTokens) {
        const auto tokens = AccountHelper::loginTokens();
        cout << "Token 1: " << tokens[0] << endl;
        cout << "Token 2: " << tokens[1] << endl;
    }

    return 0;
}
