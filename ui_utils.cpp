#include <string.h>

#include <ui_utils.h>

#include <string>
#include <algorithm>
#include <sstream>

using namespace std;

void ui_utils::print_base_screen(WINDOW *w)
{
    printw("ComunicTerm");	/* Print Hello World		  */


    // Get screen size
    int row, col;
    getmaxyx(w, row, col);

    const string msg = "Light client";
    mvprintw(0, (col-static_cast<int>(msg.length())), msg.c_str());
}

void ui_utils::alert(WINDOW *win, const string &msg)
{
    erase();
    print_base_screen(win);

    int rows, cols;
    getmaxyx(win, rows, cols);

    int start = (rows/2) - 3;

    mvwprintw(win, start, (cols-static_cast<int>(msg.length()))/2, msg.c_str());

    wattron(stdscr, A_REVERSE);
    mvwprintw(win, start+3, (cols-4)/2, " OK ");
    wattroff(stdscr, A_REVERSE);



    curs_set(0);
    refresh();

    while(getch() != 10);

    curs_set(1);
    erase();
    refresh();
}


void print_in_middle(WINDOW *win, int starty, int startx, int width, char *string, chtype color)
{	int length, x, y;
    float temp;

    if(win == nullptr)
        win = stdscr;
    getyx(win, y, x);
    if(startx != 0)
        x = startx;
    if(starty != 0)
        y = starty;
    if(width == 0)
        width = 80;

    length = static_cast<int>(strlen(string));
    temp = (width - length)/ 2;
    x = startx + static_cast<int>(temp);
    wattron(win, color);
    mvwprintw(win, y, x, "%s", string);
    wattroff(win, color);
    refresh();
}

void ui_utils::remove_special_chars(string &input)
{
    input.erase(std::remove_if(input.begin(), input.end(),
        [](char c) { return !std::isspace(c) && !std::isalpha(c); } ),
        input.end());
}
