/**
 * Information about a conversation
 *
 * @author Pierre HUBERT
 */

#pragma once


#include <string>
#include <vector>

#include "user.h"

class Conversation
{
public:
    Conversation();

    int iD() const;
    void setID(int iD);

    int iD_Owner() const;
    void setID_Owner(int iD_Owner);

    int lastActive() const;
    void setLastActive(int lastActive);

    std::string name(UsersList users);
    std::string name() const;
    void setName(const std::string &name);
    bool hasName() const;

    bool following() const;
    void setFollowing(bool following);

    bool sawLastMessage() const;
    void setSawLastMessage(bool sawLastMessage);

    std::vector<int> members() const;
    void setMembers(const std::vector<int> &members);

private:
    int mID;
    int mID_Owner;
    int mLastActive;
    std::string mName;
    std::string mComputedName = "";
    bool mFollowing;
    bool mSawLastMessage;
    std::vector<int> mMembers;
};
