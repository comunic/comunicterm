#include "user.h"

User::User()
{

}

User::User(int id, std::string firstName, std::string lastName)
    : mID(id), mFirstName(firstName), mLastName(lastName)
{

}

int User::iD() const
{
    return mID;
}

void User::setID(int iD)
{
    mID = iD;
}

std::string User::firstName() const
{
    return mFirstName;
}

void User::setFirstName(const std::string &firstName)
{
    mFirstName = firstName;
}

std::string User::lastName() const
{
    return mLastName;
}

void User::setLastName(const std::string &lastName)
{
    mLastName = lastName;
}

std::string User::fullName() const
{
    return mFirstName + " " + mLastName;
}
