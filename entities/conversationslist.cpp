#include "conversationslist.h"

using namespace std;

ConversationsList::ConversationsList()
{

}

set<int> ConversationsList::usersList() const
{
    set<int> list;

    for(auto conv : *this) {
        list.insert(conv.iD_Owner());

        for(auto m : conv.members())
            list.insert(m);
    }

    return list;
}
