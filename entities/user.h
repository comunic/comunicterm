/**
 * Single user information
 *
 * @author Pierre HUBERT
 */

#pragma once

#include <string>
#include <map>

class User
{
public:
    User();
    User(int id, std::string firstName, std::string lastName);

    int iD() const;
    void setID(int iD);

    std::string firstName() const;
    void setFirstName(const std::string &firstName);

    std::string lastName() const;
    void setLastName(const std::string &lastName);

    std::string fullName() const;

private:
    int mID;
    std::string mFirstName;
    std::string mLastName;
};

typedef std::map<int, User> UsersList;
