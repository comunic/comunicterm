/**
 * List of conversations
 *
 * @author Pierre HUBERT
 */

#pragma once

#include <set>

#include "conversation.h"


class ConversationsList : public std::vector<Conversation>
{
public:
    ConversationsList();

    std::set<int> usersList() const;
};
