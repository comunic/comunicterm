#include <iostream>

#include "conversation.h"

#include "../helpers/accounthelper.h"

using namespace std;

Conversation::Conversation()
{

}

int Conversation::iD() const
{
    return mID;
}

void Conversation::setID(int iD)
{
    mID = iD;
}

int Conversation::iD_Owner() const
{
    return mID_Owner;
}

void Conversation::setID_Owner(int iD_Owner)
{
    mID_Owner = iD_Owner;
}

int Conversation::lastActive() const
{
    return mLastActive;
}

void Conversation::setLastActive(int lastActive)
{
    mLastActive = lastActive;
}

std::string Conversation::name(UsersList users)
{
    if(hasName())
        return mName;

    if(mComputedName.size() == 0) {
        size_t i = 0;
        for(size_t j = 0; i < 3 && j < mMembers.size(); j++) {
            auto userID = mMembers[j];

            if(userID == AccountHelper::userID())
                continue;

            if(mComputedName.length() > 0)
                mComputedName += ", ";

            mComputedName += users[userID].fullName();
            i++;

        }

        if(i < mMembers.size() - 2)
            mComputedName += ", ...";
    }

    return mComputedName;
}

std::string Conversation::name() const
{
    return mName;
}

void Conversation::setName(const std::string &name)
{
    mName = name;
}

bool Conversation::hasName() const
{
    return mName.size() > 0;
}

bool Conversation::following() const
{
    return mFollowing;
}

void Conversation::setFollowing(bool following)
{
    mFollowing = following;
}

bool Conversation::sawLastMessage() const
{
    return mSawLastMessage;
}

void Conversation::setSawLastMessage(bool sawLastMessage)
{
    mSawLastMessage = sawLastMessage;
}

std::vector<int> Conversation::members() const
{
    return mMembers;
}

void Conversation::setMembers(const std::vector<int> &members)
{
    mMembers = members;
}
