/**
 * UI utilities
 *
 * @author Pierre HUBERT
 */

#include <ncurses.h>

#include <string>

#pragma once

namespace ui_utils {
    void alert(WINDOW *win, const std::string &msg);
    void print_base_screen(WINDOW *w);
    void remove_special_chars(std::string &input);
}

void print_in_middle(WINDOW *win, int starty, int startx, int width, char *string, chtype color);
