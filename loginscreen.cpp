#include <ncurses.h>
#include <string.h>

#include <string>
#include <iostream>

#include <helpers/accounthelper.h>
#include <helpers/userhelper.h>

#include "loginscreen.h"
#include "ui_utils.h"
#include "mainmenu.h"

using namespace std;

LoginScreen::LoginScreen()
{

}

enum CursorPos {
    EMAIL = 0,
    PASSWORD = 1,
    SUBMIT = 2
};

bool LoginScreen::exec(bool startApp)
{
    initscr();			/* Start curses mode 		  */
    keypad(stdscr, TRUE); // Listen to function keys
    noecho();
    cbreak();	/* Line buffering disabled. pass on everything */


    LoginResult res;
    do {
        string email, password;
        getCredentials(email, password);

        res = AccountHelper::Login(email, password);
        clear();

        switch(res) {
            case ERROR:
                ui_utils::alert(stdscr, "An error occured while trying to sign in!");
                break;

            case TOO_MANY_ATTEMPTS:
                ui_utils::alert(stdscr, "Too many attempt! Please try again later...");
                break;

            case BAD_PASSWORD:
                ui_utils::alert(stdscr, "Email / password invalid!");
                break;

            case SUCCESS:
                break;


        }
    } while(res != SUCCESS);


    try {
       const auto user = UserHelper::GetSingle(AccountHelper::userID());
       ui_utils::alert(stdscr, "Welcome " + user.fullName() + "!");
    } catch (...) {
        ui_utils::alert(stdscr, "Could not get user info!");
    }

    endwin();			/* End curses mode		  */

    if(startApp)
        showMainMenu();

    return true;
}

void LoginScreen::getCredentials(string &email, string &pass)
{
    int currPos = 0;
    int currX = 0, currY = 0;

    bool stop = false;

    do {

        erase();

        ui_utils::print_base_screen(stdscr);

        int row, col;
        getmaxyx(stdscr, row, col);

        int startX = (row-4*2)/2;
        int startY = (col-50)/2;

        mvprintw(startX, startY, "Please login to your Comunic account:");




        // Ask for email
        if(currPos == EMAIL) wattron(stdscr, A_REVERSE);
        mvprintw(startX+2, startY, "Email address:");
        wattroff(stdscr, A_REVERSE);
        printw(" ");
        printw(email.c_str());
        if(currPos == EMAIL) getyx(stdscr, currY, currX);


        // Ask for password
        if(currPos == PASSWORD) wattron(stdscr, A_REVERSE);
        mvprintw(startX+4, startY, "Password:");
        wattroff(stdscr, A_REVERSE);
        printw(" ");
        for(size_t i = 0; i < pass.length(); i++) printw("*");
        if(currPos == PASSWORD) getyx(stdscr, currY, currX);

        // Validate button
        if(currPos == SUBMIT) wattron(stdscr, A_REVERSE);
        mvprintw(startX+6, startY, "Login");
        wattroff(stdscr, A_REVERSE);

        if(currPos == SUBMIT) {curs_set(0);} else {curs_set(1);}

        move(currY, currX);

        refresh();

        int c = wgetch(stdscr);

        switch(c) {
            case KEY_UP:
                currPos--;
                if(currPos < 0) currPos = 0;
                break;

            case KEY_DOWN:
            case 9: // TAB
                currPos++;
                if(currPos > 2) currPos = 0;
                break;

            case 10: // ENTER
                if(currPos == EMAIL)
                    currPos++;
                else {
                    if(email.length() < 3 || pass.length() < 3)
                        ui_utils::alert(stdscr, "Please complete all the field before submitting form!");
                    else
                        stop = true;
                }
                break;


            case 263: // Erase
                if(currPos == EMAIL && email.length() > 0)
                    email.pop_back();
                else if(currPos == PASSWORD && pass.length() > 0)
                    pass.pop_back();
                break;

            default:
                if(c > 126) break; // We can not process this value

                if(currPos == EMAIL)
                    email += static_cast<char>(c);
                else if(currPos == PASSWORD)
                    pass += static_cast<char>(c);
        }

    } while(!stop);
}
