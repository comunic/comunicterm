#include <string.h>

#include <menu.h>
#include <curses.h>

#include <iostream>
#include <vector>

#include "helpers/userhelper.h"
#include "conversations_screen.h"
#include "helpers/conversationshelper.h"
#include "entities/conversation.h"
#include "entities/conversationslist.h"
#include "ui_utils.h"

using namespace std;

// Show the screen to choose a conversation
static int ChooseConv(ConversationsList &list, const UsersList &users) {

    ITEM **my_items;
    int c;
    MENU *my_menu;
    WINDOW *my_menu_win;
    int n_choices;

    /* Initialize curses */
    initscr();
    start_color();
    cbreak();
    noecho();
    keypad(stdscr, TRUE);
    init_pair(1, COLOR_RED, COLOR_BLACK);
    init_pair(2, COLOR_CYAN, COLOR_BLACK);


    erase();

    /* Create the window to be associated with the menu */
    /* We must do this before creating the menu to get maximum
     * size of menu entries names */
    int w,h;
    getmaxyx(stdscr, w, h);
    int numlines = w - 20, numcols = h-20;
    my_menu_win = newwin(numlines, numcols, (w-numlines)/2, (h-numcols)/2);
    keypad(my_menu_win, TRUE);


    /* Create items */
    vector<string> itemsStr;
    size_t numItems = 0;
    n_choices = static_cast<int>(list.size()) + 2;
    my_items = static_cast<ITEM **>(calloc(static_cast<size_t>(n_choices), sizeof(ITEM *)));

    // Go back
    my_items[0] = new_item("Go back", "Go back");

    // Conversations
    for(size_t i = 0; i < list.size(); ++i) {
        std::string name = list[i].name(users);
        ui_utils::remove_special_chars(name);
        itemsStr.push_back(name);
        itemsStr.push_back(to_string(list[i].members().size()) + " members");
    }

    for(size_t i = 0; i < list.size(); ++i) {
        my_items[i+1] = new_item(itemsStr.at(numItems).c_str(),
                                 itemsStr.at(numItems+1).c_str());
        numItems+=2;
    }

    my_items[n_choices-1] = new_item(nullptr, nullptr);

    /* Create menu */
    my_menu = new_menu(static_cast<ITEM **>(my_items));


    /* Set main window and sub window */
    set_menu_win(my_menu, my_menu_win);
    set_menu_sub(my_menu, derwin(my_menu_win, numlines-4, numcols-2, 3, 1));
    set_menu_format(my_menu, numlines-4, 1);

    /* Set menu mark to the string " * " */
    set_menu_mark(my_menu, " * ");

    /* Print a border around the main window and print a title */
    box(my_menu_win, 0, 0);
    print_in_middle(my_menu_win, 1, 0, numcols, "Conversations", COLOR_PAIR(1));
    mvwaddch(my_menu_win, 2, 0, ACS_LTEE);
    mvwhline(my_menu_win, 2, 1, ACS_HLINE, numcols-2);
    mvwaddch(my_menu_win, 2, numcols-1, ACS_RTEE);

    /* Post the menu */
    post_menu(my_menu);
    wrefresh(my_menu_win);

    attron(COLOR_PAIR(2));
    mvprintw(LINES - 2, 0, "");
    mvprintw(LINES - 1, 0, "Arrow Keys to navigate");
    attroff(COLOR_PAIR(2));
    refresh();

    bool stop = false;
    int numChoice = 0;
    while(!stop)
    {
        c = wgetch(my_menu_win);
        switch(c)
            {	case KEY_DOWN:
                menu_driver(my_menu, REQ_DOWN_ITEM);
                if(numChoice < list.size())
                    numChoice++;
                break;
            case KEY_UP:
                menu_driver(my_menu, REQ_UP_ITEM);
                if(numChoice > 0)
                    numChoice--;
                break;
            case 10:
                stop = true;
        }
        wrefresh(my_menu_win);
    }


    /* Unpost and free all the memory taken up */
    unpost_menu(my_menu);
    free_menu(my_menu);
    for(int i = 0; i < n_choices; ++i)
            free_item(my_items[i]);
    endwin();

    return numChoice - 1;
}


static void showConversation(Conversation &c, UsersList &users) {
    // TODO : implement screen
}


void showConversationsScreen()
{

    bool stop = false;

    while(!stop) {

        // Get information from an online source
        auto list = ConversationsHelper::GetList();
        auto users = UserHelper::getMultiple(list.usersList());

        // Get the conversation to show
        int idConv = ChooseConv(list, users);

        if(idConv < 0) {
            stop = true;
            break;
        }

        // Open the conversation
        showConversation(list[static_cast<size_t>(idConv)], users);
    }
}
